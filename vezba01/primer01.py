import sys

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("Error")
        sys.exit()
    key = sys.argv[1]
    val = [(i, i + 1) for i in range(0, 10, 2)]
    d = {}
    d[key] = val
    print(d)
