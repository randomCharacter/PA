# PA

Rešenja zadataka sa vežbi predmeta "Projektovanje algoritama" koje su rađene 2018. godine na Fakultetu Tehničkih nauka u Novom Sadu. 

Ostatak materijala, kao i originalni tekstovi zadataka mogu se pronaći na [sajtu predmeta](http://www.rt-rk.uns.ac.rs/predmeti/e2/pa-projektovanje-algoritama)
